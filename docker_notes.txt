# Docker notes

# ===================== #
# == DOCKER COMMANDS == #
# ===================== #

# run
docker run <docker_image>

# list active containers
docker ps

# list all containers
docker ps -

# stop docker (provide docker id or name
docker stop <docker_name>

# remove container
docker rm <docker_name>

# remove all containers
docker container rm $(docker container ls -aq)
https://phoenixnap.com/kb/remove-docker-images-containers-networks-volumes

# list docker images
docker images

# remove docker image
docker rmi <img_id>

# pull only the image not the container
docker pull <img_name>

# exec command
docker exec disracted_mcclintock cat /etc/hosts

# ================ #
# == DOCKER RUN == #
# ================ #

# Tag
docker run redis:4.0

# accepting input and displaying label. Combine the togeter (-it)
docker run -i <docker_image> # interactive mode to accept user input
docker run -t <docker_image> # attach to terminal to display label

# port mapping
docker run -p 80:5000 <docker_image>

# create backup/dump of data. /op/datadir is the destination
docker run -v /opt/datadir:/var/lib/mysql mysql

# inspect container. Also a way to get docker ip address
docker inspect <container name or id> 

# check logs
docker logs <container_name or id>

# run docker in the background
docker run -d <image> <params>

# attach docker
docker attach <container_id>

# set environmental variable
docker run -e APP_COLOR=<value>

# How to set container name
docker run --name <container_name> <docker_image>

# ================================ #
# == HOW TO CREATE DOCKER IMAGE == #
# ================================ #

$ docker run -it debian bash

# Install dependencies
apt-get install update 
apt-get install -y python3 
apt-get install -y python3-pip
pip3 install flask

# Use this sample code from https://github.com/mmumshad/simple-webapp-flask/blob/master/app.py then ctrl + c
cat > /opt/app.py  

# Run flask in terminal
FLASK_APP=/opt/app.py flask run --host=0.0.0.0

# Check container ip address by using 'docker inspect <container_id>' check on the 'network > IPaddress' section
# Open a browser the enter the <ip:5000> in the URL
# Congrats! You successfully created a docker image. 

# ********************************************************
# Now, How to dockerize it?
1 . Create a file named 'Dockerfile' then write these to install dependencies 

# ========================================================= #
FROM debian

RUN apt-get update 
RUN apt-get install -y python3 python3-pip
RUN pip3 install flask

COPY app.py /opt/app.py

ENTRYPOINT FLASK_APP=/opt/app.py flask run --host=0.0.0.0
# ========================================================= #

# make sure they are in the same directory with your project pp files (app.py for example)

$ docker build . -t <username/image_name>

# How to push it to docker hub
docker login
docker push <image_name>

# docker CMD
CMD <command in docker file>
CMD ["command", "param1"]
CMD ["sleep", "5"]

# docker ENTRYPOINT
ENTRYPOINT 
ENTRYPOINT ["command", "param1"]
ENTRYPOINT ["sleep", "5"]

# ==================== #
# == DOCKER COMPOSE == #
# ==================== #

# Docker compose (YAML) is a configuration file (docker-compose.yml)
# To enable config file, execute "docker-compose up" command

# How to link containers
docker run --link <container_name:host_name>

# sample docker composition

docker run -d --name=redis redis
docker run -d --name=db postgres:9.4
docker run -d --name=vote -p 5000:80 --link redis:redis voting-app
docker run -d --name=result -p 5001:80 --link db:db result-app
docker run -d --name=worker --link db:db --link redis:redis worker

# docker-compose.yml
redis:
	image: redis
db:
	image: postgres:9.4
vote:
	image: voting-app
	ports:
		- 5000:80
	links:
		- redis
result:
	image: result-app
	ports:
		-5001:80
	links:
		-db # same to db:db
worker:
	image: worker
	links:
		- redis
		- db

# ====================================


# ===================== #
# == DOCKER REGISTRY == #
# ===================== #


# docker private registry
docker run -d -p 5000:5000 --name registry registry:2
docker image tag my-image localhost:5000/my-image
docker push localhost:5000/my-image		# push to my own host
docker pull localhost:5000/my-image		# pull from my own host
docker pull 10.10.10.20:5000/my-image 	# pull from another host

# =========================================== #
# == DOCKER ENGINE, STORAGE and NETWORKING == #
# =========================================== #
































































































