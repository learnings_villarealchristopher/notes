# customizing your terminal (not working on default terminal)
# .bash_profile is not created by default
# \d Date in "Weekday Month Data" format "Tue May 26"
# \h Hostname up to the first period
# \H Hostname
# \n Newline
# \t Current time in 24hr HH:MM:SS format
# \T Current time in 12hr HH:MM:SS format
# \@ 12 hr am/pm format
# \A 24hr HH:MM format
# \u username of current user
# \w current working directory
# \W Basename of the current working directory
# \$ if the effective UID is 0, a #, otherwise a $
# check man bash for other options
export PS1="[\u@\h/\W]\% "


# adding alias 
# should be added in ~/.bashrc instead
alias cls='clear'
alias vi='vim'

# printenv to show all ENVIRONMENTAL VARIABLES
# add env variables in ~/.bash_profile for persistency
export VAR="value"
echo $VAR
unset VAR

