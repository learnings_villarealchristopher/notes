ps

-e	Everything, all processes
-f	Full format listing
-u	Display username's processes
-p pid	Display information for PID

# common commands
ps -e		Display all processes
ps -ef		Display all processes full
ps eH		Display a process tree
ps -e --forest	Display a process tree
ps -u username	Display users' processes

# other ways to view processes
pastree		Display processes in a treee format
top		Interactive process viewer
htop		Interactive process viewer

# background & foreground processes
command &	Start command in background
	> ./program.sh &
Ctrl+c		Kill the foreground process
Ctrl+z		Suspend the foreground process

bg [%num]	Background a suspended process
fg [%num]	Foreground a background process
kill		Kill a process by job number or PID
jobs [%num]	List Jobs

# kill processes
Ctrl+c			Kills the foreground proc
kill [-sig] pid		Send a signal to a process	
kill -l			Display a list of signalsi

# examples
# default kill signal is "TERM" = -15
# if the process is not killed by -TERM or -15, use kill -9 123
kill 123
kill -15 123
kill -TERM 123	

# - sign represents previous job
# + sign represents current job
[1]- Running	./program.sh &
[2]+ Running	./program.sh &

# display jobs
jobs		List all jobs 
jobs %%		Shows current job
jobs %+		Shows current job
jobs %-		Shows previous job

